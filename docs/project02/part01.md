# Project 2 

## Part 01 of 01

Use the knowledge and skills to explore and learn from existing resources

### Context & Goal

You've just entered a new role at your company, your supervisor would like you to dive in and level up your knowledge. Engage in the provided training materials (in a corporate setting this might be a day or two, maybe a week).

### Objective - Part 1 of 1:
Student/learners will be able to:
1. leverage existing skills and knowledge to continue to grow and build their knowledge

### Preliminaries

You have two choices: 
1. VCS QuickStart Guide 
   * HTML Guide ([located here](https://spdocs.synopsys.com/dow_retrieve/latest/vg/VCS/QuickStart/Responsive_HTML5/index.htm))
   * Required Code ([MinSoc.tar.gz](https://spdocs.synopsys.com/dow_retrieve/latest/vg/VCS/QuickStart/MinSoc.tar.gz))
   * FOR UCF Students ONLY: `cp -rf /home/net/mi029136/public/proj2 proj2`
      *  `cd proj2`
      *  `tar -xvf MinSoc.tar.gz`
      *  `cd MinSoc`
      * Follow Instructions until compilation breaks (fails); don't forget to change you shell `csh` first
      * Bonus Instructions - Use Dr. Mike's Simulation Directory to get the result of `make SOC_Verification`
         1. Get yourself back to the project 2 directory: `cd ~/proj2`
         2. Extract Dr. Mike's simulation: `tar -xvf sim.tar.gz`
         3. Copy the simulation results into the MinSoC directory: `cp -rf [the directory structure that was just extracted]/sim/* MinSoC/sim/.`
         4. Reenter Simulation: `cd MinSoC/Sim` ;
           *  can you access `.simv` ? or perhaps
           *  `make sim_debug_interactive`

2. Language: SystemVerilog Verification Using UVM Training ([located here](https://training.synopsys.com/learn/course/1683/language-systemverilog-verification-using-uvm))
   * If this link doesn't work start [here](https://training.synopsys.com/pages/47/university-program)
   * Search for "UVM"
   * Enroll (it should be $0 dollars - if it's not, make sure you sign in first!)


You may work in groups of at max 3. Groups may collaborate, but every group must turn in their own report.

Everything is due by 12:50 PM (this is mid-day, not evening) on December 5th 2023 

You may NOT finish your choice - get as far as you can. During reflection - discuss how far you got and why.

### Project Submission

You and your team will submit the following (due by the end of our final exam time [12:50 PM on Dec 05 2023]):
1. a project report detailing evidence of what you did and how you did it;
2. any code/results your generated (as a zip file);
3. a short (<5 minute) video recording from the group about what you learned, what worked well, and what you would do differently next time;

