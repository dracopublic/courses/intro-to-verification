# Working [Remotely] With Linux

Note about style - **bolded** words are terms, code, terminal examples, and commands to copy and execute verbatim are `mono-spaced` and colorized. Items in angle brackets `<>` indicate a variable term that need to be replaced by a specific value, square brackets `[]` indicate an optional term.

## Foundational Terminology

- **Linux** : An open-source **kernel** (software that connects your operating system with the hardware) developed orginally by Linus Torvalds
- **Distributions** or Distros : Operating Systems which use the Linux kernel
- **Shell** : Program which enables direct text-based interacation with the opperating system
- **Terminal** : A program that enables you to interact with one or more shells

- **Host** : A machine that communicates with other machines (hosts) on a network 
- **Hostname** : What a host is called within a network; the name of a specific machine; e.g., vlsi; often includes the domain name (see below). e.g., vls3i.eecs.ucf.edu, en.wikipedia.com
- **Domain Name**: A bit more complex, but for our purposes describes a grouping (realm) of control - e.g., a network of machines, eecs.ucf.edu, wikipedia.com, ucf.edu

- **Local** host: The machine you are currently logged into and using
- **Remote** host: The machine that you are accessing from the current machine

Why does this matter? Some of the commands we'll run require us to state which machine is local vs. remote.

It's important to note that the context for local vs. remote changes depending on which system you are currently using. 

*The frame of reference of local versus remote depends on the system you are currently accessing - not the system you are phyiscally working from*. 

> ### Scenario: 
>Let's imagine two systems, your physical system (let's call it Alpha) and a system located somewhere else (Draconis). We'll run through a typical sequence of connecting and moving information between the two to see the distinction. 
>1. Login to Alpha, Open tools etc.
>  - Local host = Alpha
>2. Initiate a connection to host named (Draconis)
>  - Examples: Web requests (going online, initiating an SSH connection)
>  - Local host = Alpha
>  - Remote host = Draconis
>3.  After connecting (via SSH) to Draconis performing opperations on the machine
>  - Local host = Draconis
>  - Remote host = Alpha



## Linux Foundations


### Getting Started / Help



The the programs/commands shown should be executed on the command line. 

The command line is capable of running a single command at a time, typically and unless otherwise instructed, in the foreground of the terminal you're in. The terminal provides you a prompt to know you can input a command and this prompt it configurable and varies from systems to system. For example on the systems we'll be using for class the command prompt is indicated as follows:

> `[<NID>@<HOSTNAME> <DIR>]`


Many tutorials show the prompt as either a **>** or **#** 

If needing additional help with a command you can attempt to refer to the commands manual page using the syntax `man <command_name>` replacing `<command_name>` with the name of the tool/program you wish to know more about. 



> Example
> It is common to want to clear you terminal window of text/contents. The unix command for that is `clear` so on the command line 
>
> If you want to learn more about `clear`:
>
>  `man clear`
>
> Results in the following response (edited for length):

```bash
clear(1)                                      General Commands Manual                                     clear(1)

NAME
       clear - clear the terminal screen

SYNOPSIS
       clear [-Ttype] [-V] [-x]

DESCRIPTION
       clear  clears  your screen if this is possible, including its scrollback buffer (if the extended “E3” capa‐
       bility is defined).  clear looks in the environment for the terminal type given by the environment variable
       TERM, and then in the terminfo database to determine how to clear the screen.
<...>
 Manual page clear(1) line 1 (press h for help or q to quit)

```
### Filesystem

Organization in a text-based environment is critical. Understanding the basic directory structure and layout of where files are located will help you navigate and do your work. 
#### The basics:

* The filesystem is a tree which starts at the **root** (`/`). 
* When you login to your system, you're generally placed within your **home directory**  (e.g., `/home/<username>`, `/home/net/username`, or some other scheme dictated by your system administrator).
* You can view the structure of the directory tree using the command `tree [directory]`, using it this way shows you everything in the tree, this can be overwhelming as it will show you every single file starting from the directory that you listed that you have permissions to see/access. Depending on if and where you ran the `tree` command you might find yourself waiting a while, press `ctrl-c` several times to interupt (kill) the `tree`` **process**
* Instead of viewing the entire depth of the tree, you can limit the levels (depth) that tree will report by providing and **optional parameter** often indicated using square bracket `tree [-L level] [directory]` - keen observers will note that the directory is actually an optional parameter, use the manual pages `man` to learn more. Example:\
 `tree -L 1 /`

```bash
/
├── bin -> usr/bin
├── boot
├── dev
├── etc
├── home
├── lib -> usr/lib
├── lib64 -> usr/lib64
├── media
├── mnt
├── net
├── opt
├── proc
├── root
├── run
├── sbin -> usr/sbin
├── srv
├── sys
├── tmp
├── usr
└── var
```

  

#### Navigation

* You traverse through the tree by *changing directories* 
 which is accomplished by providing a **path** or location that you wish to move to.
   * `cd [path]`
* A **path** can be any location (that you have permissions to access) within the tree. 

##### Where am I?
Locate your *present working directory* using `pwd` 

##### Getting Home
I've gotten myself lost, how do I get home? Use any of the following:
  * `cd` 
  * `cd ~`
  * `cd /home/net/<nid>` [ucf machine specific]


For the follwing, imagine the following directory structure is located at `/home/net/<nid>/demo`:

```bash
[<nid>@vlsi demo] tree
.
├── courses
│   └── intro-to-verif
│       └── examples
├── examples
├── homework
└── research
```


  * You can access a path directly if you know it's exact location relative to the root:\
  `cd /<folder_Level1>/<folder_Level2>/....`
    * So, if we wanted to directly access the `examples` folder in the `intro-to-verif`  folder, and we know the tree above is located at `/home/net/<nid>/demo/` we could navigate to our desired directory using an absolute path from root (`/`):\
    `cd /home/net/<nid>/demo/courses/intro-to-verif/examples`
 
 This approach might not be the easiest - memorizing the entire structure and navigating to a location directly generally only occurs after you are familiar with the space.

 More realistic - traversing the tree a few levels at a time:

  * Moving Forward (deeper / down  within the tree):
     * You can also access a directory based on where you currently are by omitting the leading `/`\
     `cd <folder_levelX>`
     * Assuming you were in the `demo` folder above, you could do the following theww commands:\
     `cd courses`\
     `cd intro-to-verif`\
     `cd examples`

  * Moving Backwards (back up towards the root) by using `cd .. `
     * Assuming we've made our way to\
      `/home/net/<nid>/demo/courses/intro-to-verif/examples/` 
     
     ```bash
     [<nid>@vlsi examples] pwd
     /home/net/<nid>/demo/courses/intro-to-verif/examples
     [<nid>@vlsi examples] cd ..
     [<nid>@vlsi intro-to-verif] pwd
     /home/net/<nid>/demo/courses/intro-to-verif
     ```
     * You can traverse backwards and forwards with combinations of moving backward and foward:
     * `cd ../../`. [back/up two]
     * `cd ../<new_location>` [back/up one, forward/down to another location]
     * `cd ../../<new_location_x>/new_location_x+1>` [back/up 2, fw/down two]

    





  This begs the question - how do you know where to go? 
  * You can **list** the contents of any directory `ls [directory]`
  * You can also attempt to `find` files/directories by name.\
  `find [directory] -name <search-term> `
    * When using find this way `search_term` parameter above can be:
       1.  the exact name of what you're looking for. e.g., `find -name examples` would locate the two `examples` folder but `find -name example` would not
       2. can include **wildcard** charactes like `*` (match anything) or `?` (match single character)
          * `find -name ?xample?` would locate folders such as `Example, example, Examples, examples, xampled ...`
          * `find -name *.txt` would locate all files from the current direction with the `.txt` extension
          * `find -name *example*.v` would locate all files    
  



#### Manipulation 

##### Files
* Create new files > `touch <filename>`
* Move file > `mv <current_file(s)> <new_location>`
* Copy file > `cp <current_file(s)> <new_name>`
* Remove file [WARNING] > `rm <current_file(s)`

###### Editing

On almost every system you should have access to `nano` and `vim` but all the amazing developers really use `emacs` which is not always installed by default [^1] [^2] [^3].
The simplest to use is `nano`.

* Usage: `nano [filename]`
* Modeless editor, just type, use `ctrl+<key>` to navigate and perform actions
* [See tutorial here](https://linuxize.com/post/how-to-use-nano-text-editor/)





##### Directories
* Create new directory > `mkdir <directory_name`
* Move directory > `mv -r <current_location> <new_location>`
* Copy directory > `cp -rf <current_location> <new_location>`



## Remote Considerations

### Background Tasks
When running commands on the command line they are typically being run in the foreground. What if you wanted to do something in the background while working on something else?

Use `&` at the end of your command to send jobs to the background.

Want to see your background jobs? Use `jobs -l`

Need to active (bring a job to the foreground)? 
* Only have one background job?  `fg`
* Have >1? `fg %<job#>` 

### Persistence

We're working on a remote connection. What happens if the connection between the machines fails? 

Use `screen` 
1. Create a new screen session: `screen -S myproject-classx`
2. You can leave (detach yourself) from the new session any time `ctrl-a d` 
3. You can return (reattach yourself) to the session any time (even if you get disconnected) `screen -r`
4. You can have multiple screen sessions, list them `screen -ls`
   * To reattach you must specify the process identifier shown in the listing\
    `screen -r <PID>`
5. Terminating a screen process (from within): `ctrl-a x`
Read more about `screen` [here](https://linuxize.com/post/how-to-use-linux-screen/)


# About this document

Developed by Dr. Mike Borowczak as a reference guide 

# License

This work is licensed under Attribution 4.0 International. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/



[^1]: https://en.wikipedia.org/wiki/Editor_war
[^2]: https://monovm.com/blog/emacs-vs-vim/
[^3]: https://www.redhat.com/sysadmin/3-text-editors-compared