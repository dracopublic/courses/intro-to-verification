
# Server Access

This document contains details on how to access  the UCF VLSI Linux Server located at vlsi.eecs.ucf.edu.
These instructions should hold for other servers (e.g., Eustis and VLSI3) provided that you have accounts on those machines.

## SSH 
SSH = Secure Shell / Secure Socket Shell 
This is a protocol that enables you to connect to another machine from your own.

Connection information below assumes that you are connected to ucf wifi (not guest).
If you are able to connected to ucf wifi (due to your location) scroll to the bottom of this document and first connect to the ucf VPN.

### From Linux Machine

1. Open your favorite terminal 
2. Connect to server with your netid and password with the following command
    - `ssh -X <NID>@vlsi.eecs.ucf.edu`
    -  use your UCF NID to replace the <NID> above

 
### From Mac Machine

1. Download and install Quartz (or XQuartz)
2. Connect to server with your netid and password with the following command
    - `ssh -X <NID>@vlsi.eecs.ucf.edu`
    -  use your UCF NID to replace the <NID> above


### From Windows Machine

#### Option 1: MobaXTerm (easiest, recommended)
1. Install from http://mobaxterm.mobatek.net/
2. Connect to `vlsi.eecs.ucf.edu`
   - specify username: `NID`
   - port: `22`

#### Option 2: bitvise 
1. Install from https://www.bitvise.com/ssh-client-download
2. Follow instruction from https://www.bitvise.com/ssh-x11-forwarding
3. Connect:
   - host: `vlsi.eecs.ucf.edu`
   - port: `22`
   - terminal tab:
      - enable X11 forwarding


## Remote (off-campus) 

View Dr. Szumlanski's document [here](https://www.cs.ucf.edu/courses/cop3502/fall2021/prog/cop3502-eustis-guide.pdf) which discussess connecting to another ucf servce eustis which also contains details for getting the cisco anywhere connect client installed and working.
