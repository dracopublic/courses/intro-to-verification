# Synopsys Training Site

Synopsys is one of three major EDA (Electronic Design Automation) vendors. They have an incredibly large suite of tools that aide the development, design, verification, testing, and release of today's electronic devices.

You may have  access to their training and documentation if your company or insitutition has signed agreements with them.

## Register for access to Synopsys Training Site 

1. Visit https://solvnetplus.synopsys.com/s/login/SelfRegister?language=en_US
2. Use your institutional email address (e.g., @ucf.edu )
3. Provide your Site ID Code; your administrator or instructor should provide this to your

## Materials

### Documentation

For this particular class we're going to focus on VCS; 
1. From the documentation tab find the [VCS](https://spdocs.synopsys.com/dow_retrieve/latest/home_public/vcs.html) tool
2. Within VCS area navigate to User Guides to grab a few primary resources for future easy access
   - VCS User Guide 
   - VCS Testbench Quick Start Guide
   - VCS Testbench Tutorial Suite 
3. We'll also use the [Online VCS Quick Start Guide](https://spdocs.synopsys.com/dow_retrieve/latest/vg/VCS/QuickStart/Responsive_HTML5/index.htm)
   - Which will utilize the associated [MiniSoc.tar.gz](https://spdocs.synopsys.com/dow_retrieve/latest/vg/VCS/QuickStart/MinSoc.tar.gz) files 

