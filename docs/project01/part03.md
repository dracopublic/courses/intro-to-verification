# Project 1 

## Part 03: Exploring Constrained Random Verification

Featuring VCS & Tiny ALU 

### Context & Goal
Focusing on one testbench and our tinyalu we'll explore how the power of constrained random verification.

### Preliminaries
1. Complete Project [Part 01](main/docs/project01/part01.md) and [Part 02](main/docs/project01/part02.md)
2. Part 03 Assumes:
    * `tinyalu.zip` has been extracted to `~/courses/eel4932fa23/project1/tinyalu`
3. You have the documentation readliy available:
    * Skim Synopsys Documentation @ [Synopsys VCS Documentation](https://spdocs.synopsys.com/dow_retrieve/latest/home_public/vcs.html) --> [VCS Documentation - HTML Help](https://spdocs.synopsys.com/dow_retrieve/latest/vg/VCS/vcs_olh/index.htm) 

### Project Report

You will submit one project report (per instructions on webcourses). The report consists of responses/materials from the individual parts.

### Objective - Part 3:
Student/learners will be able to:
1. Use and understand how a makefile works
2. Modify a testbench so that value selection is less (and more) constrained and observe the impact on testing certain covers
3. Modify the random number selection to pull from different distributions and observe the impact on testing certain covers

### Project Part 3 

#### Step 1
Make sure you have completed the preliminaries above, and as needed, get ready using the [getting started materials](main/docs/getting-started) and [completing part 01](main/docs/project01/part01.md) and [part 02](main/docs/project01/part02.md)

#### Step 2
1. Change directory to  `~/courses/eel4932fa23/project1/tinyalu/03_Interfaces_and_BFMs`
2. Create a new file called `makefile`
    1. `nano makefile` (replace nano with your favorite editor?)
    2. Copy these contents into that new file - note that indented lines are **TABs NOT spaces**! 
	```makefile
	#Learn more about Makefiles at https://makefiletutorial.com/
	elaborate: dut tb
		vcs '-timescale=1ns/1ns' -kdb -full64 -debug_access+all top -lca +vcs+dumpvars  -assert funchier+svaext -cm line+cond+fsm+tgl+assert -sverilog

	dut:
		vhdlan -kdb -full64 -verbose -f dut.f

	tb:
		vlogan -kdb -full64 -verbose -sverilog -f tb.f +define+ASSERT_ON+COVER_ON

	clean:
		rm -rf 64/ csrc/ AN.DB/ simv* verdi_config_file work.lib++ vc_hdrs.h *~
	```


#### Step 3:

Test/use your newly created makefile.
1. Try `make clean` on the command line; what happens (what commands are run)?
2. Try `make tb`; what happens (what commands are run)?
3. `make clean`
4. `make elaborate`; what happens (what commands are run)?
5. `make clean`
6. `make all` or `make` ; what happens (what commands are run)?


#### Step 4:

* **Simulate & Generate Report**
    1. `./simv -cm line+assert+cond+fsm+tgl `
    2. Type `q` and press `enter` to quit
    3. Let's create a directory for all our reports `mkdir reports`
    4. Generate a report inside that directory with a special name (`baseline`) \
	`urg -dir simv.vdb/ -metric group -report reports/baseline -format both` 
    5. View the generated reports (html or text):
		1. `cd reports/baseline`
		* Text: `more *.txt` (which html file do you think would be most useful?)
		* HTML: `firefox dashboard.html`
	6. Even using firefox over -X is slow - let's copy these files!
		* On your Localhost: either open a file transfer tool or use command line
			* UNIX/MAC:  Use secure copy: `scp -r <nid>@vlsi.eecs.ucf.edu:/home/net/<nid>/courses/eel4932fa23/project1/tinyalu/03_Interfaces_and_BFMs/reports proj1Reports`
		* After copying the files locally, view them using your favorite browser!
		
* Automation
	1. How can we automate this report generation in the future? Makefile!
    2. Modify your makefile that's located in the `03_Interfaces_and_BFMs` directory (`nano makefile`) to include these contents (or copy the makefile directly, see below:)

```makefile
#Learn more about Makefiles at https://makefiletutorial.com/
NAME=baseline

all: elaborate
.PHONY: all

sim: elaborate
	./simv -cm line+assert+cond+fsm+tgl

elaborate: dut tb
	vcs '-timescale=1ns/1ns' -kdb -full64 -debug_access+all top -lca +vcs+dumpvars  -assert funchier+svaext -cm line+cond+fsm+tgl+assert -sverilog

dut:
	vhdlan -kdb -full64 -verbose -f dut.f

tb:
	vlogan -kdb -full64 -verbose -sverilog -f tb.f +define+ASSERT_ON+COVER_ON

report:
	urg -dir simv.vdb/ -metric group -report reports/$(NAME) -format both

clean:
	rm -rf 64/ csrc/ AN.DB/ simv* verdi_config_file work.lib++ vc_hdrs.h *~

dataclean:
	rm -rf reports

cleanall: clean dataclean

```


This makefile is available [here](https://gitlab.com/dracopublic/courses/intro-to-verification/-/blob/main/docs/project01/makefile) or from our server copy directly using `cp ~mi029136/public/proj1/makefile .`


#### STEP 5 : Cleaning up (again)

 * **Inline Lab Question #1:**
1. What is the difference between `make dataclean` and `make clean`? What does `make cleanall` do?
2. Run `make clean` - what directory remains? Why is that useful?

####  Step 6: Exploring Randomness 

We will be modifying the contents of the SystemVerilog files used to run the testbench -  **we should make a backup**.
1. `mkdir bak`
2. `cp *.sv bak/.`

Let's explore where randomness occurs:
1. Where does the word random appear in our system verilog files?\
`grep -l random *.sv`
2. Open up the file that was reported:
`nano -l <filename from #1`

3.  **Inline Lab Question #2:** 
	* Create a table, in the first column write the  each line where the keyword `$random` occurs, in the second column write the code on that line, in the third write the variable/signal name that the random value is being placed into, and in the forth columns how many bits wide that variable/signal is, in the fifth column write the number of possible values that signal can take (2^?) and in the last column write down, if one exists, the possible mapping of these values to a smaller space of options

| Line | Code | Var/Sig/Func | Size (bits) | Possible Values | Mapping |
| :--- | ---  | ------  | :-----:     | :-------:       | :----   |
| 21   |      |         |             |                 |         |
| 36   | `zero_ones = $random` | `zero_ones` | 2 | 4        | None    |

4. **Inline Lab Question #3:** What are the possible outputs from the two main functions:
	* get_op?
	* get_data?

5. **Inline Lab Question #4:**  With the baseline code, list the theoretical probability of occurrence for the possible outputs for each function. 

* For get_op list the probabilities for each possible output, make and fill out the first row of a table like this:

| BASELINE OP TEST | <OP_x?>  | <OP_y?> | <Op_z?> | ... |
| ---      | ---   | ---- | ---  | ---- |
| theoretical  | %     | %    |  %     | %   |
| simulated  | %     | %    |  %     | %   |

* For get_data list the theoretical probabilities for each of these ranges; Generate and fill the first frow of this table for get_data theoretical probabilities:

| BASELINE DATA Run | Zeros  | Ones| 1:FE |
| ---      | ---   | ---- | ---  | 
| theoretical  | %     | %    |  %     |
| simulated  | %     | %    |  %     | 

6. Run the following commands:
	* `make cleanall`
	* `make sim`. (then quit the simulator)
	* `make report NAME=BASELINE`

You'll revisit this report AND the tables you've started later on.

#### Step 7: Relax to NO Constraints (UNCONSTRAINED)

Edit `tester.sv` so that the values generated for the legs (inputs) of the ALU are completely random. 

The function which handles this is `get_data()`:

```c
   function byte get_data();
	bit [1:0] zero_ones;
	zero_ones = $random;
	if (zero_ones == 2'b00)
        return 8'h00;
	else if (zero_ones == 2'b11)
        return 8'hFF;
	else
		return $random;
   endfunction : get_data

```

1. Comment out this entire function (we'll save it for later)
	* You can either do single line comments by placing `//` at the start of each line
	* or you can comment out the entire block by placing a `/*` before the first line and a `*/` after the last line
2. Create a new empty function block using the same signature, 
```c
	function byte get_data();
		<fill in needed lines here>
   endfunction : get_data
```
3. The only statement in this function should be a `return $random;`

4. **Inline Lab Question #5:** With this new UNCONSTRAINED version, For get_op list the theoretical probabilities for each possible output, make and fill out the first row of a table like this:

| UNCONSTRAINED OP Test  | <OP_x?>  | <OP_y?> | <Op_z?> | ... |
| ---      | ---   | ---- | ---  | ---- |
| theoretical  | %     | %    |  %     | %   |
| simulated  | %     | %    |  %     | %   |

* **Inline Lab Question #6:** For get_data list the theoretical probabilities for each of these ranges; Generate and fill the first frow of this table for get_data theoretical probabilities:

| UNCONSTRAINED Data Test  | Zeros  | Ones| 01:FE |
| ---      | ---   | ---- | ---  | 
| theoretical  | %     | %    |  %     |
| simulated  | %     | %    |  %     | 


6. Run the following commands:
	* `make sim`. (then quit the simulator)
	* `make report NAME=UNCONSTRAINED`

You'll revisit this report AND the tables you've started later on.


#### Step 8: Add Constraints

Edit `tester.sv` so that the values generated for the legs (inputs) of the ALU are less random.

First, remove the function you added in step 7. 

The function which handles random number selection is `get_data()` - you commented it out in the last step, now uncomment it:

```c
   function byte get_data();
	bit [1:0] zero_ones;
	zero_ones = $random;
	if (zero_ones == 2'b00)
        return 8'h00;
	else if (zero_ones == 2'b11)
        return 8'hFF;
	else
		return $random;
   endfunction : get_data
```

1. After uncommenting the function, add an new `else if` block before the final `else` block:
	* which checks if `zero_one == 2b'10`
	* and returns an 8 bit hex value representing your favorite number 1-254.
	* NOTE: though the valid range is 0-255 your favorite number MAY NOT be 0 or FF (e.g., 255).
	* **Inline Lab Question #7:** Document the favorite number you/your team are using.

```c
else if (zero_ones == ???)
	return 8'h??;
```

2. **Inline Lab Question #8:** With this new CONSTRAINED version, For get_op list the theoretical probabilities for each possible output, make and fill out the first row of a table like this:

| CONSTRAINED OP Test  | <OP_x?>  | <OP_y?> | <Op_z?> | ... |
| ---      | ---   | ---- | ---  | ---- |
| theoretical  | %     | %    |  %     | %   |
| simulated  | %     | %    |  %     | %   |

* **Inline Lab Question #9:** For get_data list the theoretical probabilities for each of these ranges; Generate and fill the first frow of this table for get_data theoretical probabilities:

| CONSTRAINED Data Test  | Zeros  | Ones | 01:<FAV_NUMBER_INHEX-1>,<FAV_NUMBER_INHEX+1>:FE | <FAV_NUMBER_INHEX> | 
| ---      | ---   | ---- | ---  | ---   | 
| theoretical  | %     | %    |  %     | %     | 
| simulated  | %     | %    |  %     | %     | 

But - how will we be able to track when this value is seen? We don't have a bin for it yet.

3. Modify `coverage.sv` to be able to track this new value in a bin
	* open the file `nano coverage.sv`
	* find a and b leg coverpoints (`coverpoint A` and `coverpoint B`) and **FOR BOTH**:
	* add a line in each which matches the syntax of the bin for zeros `bins zero = {'h00};` but now for your favorite number:
		* `bins favorite = {'h??};`
	* and you need to update the `others` bin:
		* `bins others = {['h01:'h<fav-1>],['h<fav+1>:FE]}`
	
4. Run the following commands:
	* `make sim`. (then quit the simulator)
	* `make report NAME=CONSTRAINED`

You'll revisit this report AND the tables you've started later on.


#### Step 9 Complete the tables

You should have six (6) partially completed data tables [BASELINE OP, BASELINE DATA, UNCONSTRAINED OP, UNCONSTRAINED DATA, CONSTRAINED OP, CONSTRAINED DATA] with theoretical values. Pull the reports to your local machine and view them to find the coverage data to complete your tables.


1. Get all of the reports at once on your local machine:
	* Linux/OSX: From your  (local) machine - use SSH Secure Copy `scp` or using an appropropriate filemanager to retrive the folder:
	`scp -r <nid>@vlsi.eecs.ucf.edu:/home/net/<nid>/courses/eel4932fa23/project1/tinyalu/03_Interfaces_and_BFMs/reports proj1part3Reports`
	* Windows Users	see [this webcourse discussion thread](https://webcourses.ucf.edu/courses/1441226/discussion_topics/7470851) for moboxterm instructions (thank you to Lana and Henry!)
2. With the reports downloaded, for each experiment/report, access the html dashboard.html file using your favorite browser, navigate to groups and find the counts associated with each group listed in your table, compute the percent chance/occurrence of the values.



### Questions:

Create a project report section for Part 3 - include details / questions / responses from completing the procedure above. At a minimum answer the **"Inline Lab Questions X {1-9}"**

After going through the procedure, also answer the following questions - you might need to spend more time with the design and tools!

Based on your exploration above, searching/research online and in the manual


1. Based on your completed tables describe the impact of relaxing (unconstrained) and restricting (further constraining) the choice of random values on the ability to cover interesting scenarios.
2. Assuming you start from the baseline code, hypothesis and describe (you're free to test it) what would happened to the op coverage and data coverage if the following code was used to generate OP codes:

```c
   function operation_t get_op();
      bit [2:0] op_choice;
      op_choice = $random;
      case (op_choice)
        3'b000 : return no_op;
        3'b001 : return add_op;
        3'b010 : return add_op;
        3'b011 : return xor_op;
        3'b100 : return mul_op;
        3'b101 : return no_op;
        3'b110 : return no_op;
        3'b111 : return no_op;
      endcase // case (op_choice)
   endfunction : get_op

```

3. Include copies of all of your tables.

Now that you have investigated and worked with two of the codebase examples use the 03 folder and compare the testbench and DUT files to the testbench show in chapter 3 of the book (see figures 3.1; 3.7). 

4. What testbench components are present in our code? Are any components missing? 

5. Would you consider the testbench in 03 self-checking? Why or Why Not?


# Fin
