# Project 1 

## Part 02: Exploring Covers and Moving towards Reuse

Featuring VCS & Tiny ALU 

### Context & Goal
You've been handed a new design with two different testbench environments. Explore the test environments including what (dut) and how (testbench) they test.
Spend time exploring the differences in the structure of the two environments.

### Preliminaries
1. Complete Project [Part 01](main/docs/project01/part01.md)
2. Part 02 Assumes
    * `tinyalu.zip` has been extracted to `~/courses/eel4932fa23/project1/tinyalu`
3. You have the documentation readily available:
    * Skim Synopsys Documentation @ [Synopsys VCS Documentation](https://spdocs.synopsys.com/dow_retrieve/latest/home_public/vcs.html) --> [VCS Documentation - HTML Help](https://spdocs.synopsys.com/dow_retrieve/latest/vg/VCS/vcs_olh/index.htm) 

### Project Report

You will submit one project report (see Webcourses for due date) The report consists of responses/materials from the individual parts.

### Objective - Part 2:
Student/learners will be able to:
1. Perform basic comparison of directories to identify key similarities/differences
2. Extend ability to run three step process from single flat testbench file to multi-file testbench
3. Explore and build understanding of different types of test coverage including:
    * built in functional coverage (covers) as well as code coverage.

### Project Part 2 

#### Step 1
Make sure you have completed the preliminaries above, and as needed, get ready using the [getting started materials](main/docs/getting-started) and [completing part 01](main/docs/project01/part01.md)

#### Step 2
Create two environment variables to quickly store the locations for the two different testbench directories.
1. Create TB1 shortcut:
    * Change directory to `~/courses/eel4932fa23/project1/tinyalu/02_Conventional_Testbench`
    * `export tb1=$(pwd)`
2. Create TB2 shortcut:
    * Change directory to  `~/courses/eel4932fa23/project1/tinyalu/03_Interfaces_and_BFMs`
    * `export tb2=$(pwd)`

To use the newly created testbench directory variables you must use a `$`. e.g. `cd $tb1`
If you would like to reuse these commands later, consider creating a `proj1.bash` file in your home directory:\
1. `nano ~/proj1/bash`
2. Include the following (replacing <username> with your username):\
```bash
export tb1=/home/net/<username>/courses/eel4932fa23/project1/tinyalu/02_Conventional_Testbench
export tb2=/home/net/<username>/courses/eel4932fa23/project1/tinyalu/03_Interfaces_and_BFMs
```
3. When working on the project run `source ~/proj1.bash`
\
#### Step 3:

Clean up the directory structure in `$tb1`:
1. `cd $tb1`
2. Remove all files/folder that don't match the pattern of files we want\
`rm -rv !(*.f|*alu*|*.do|*.vhd|*.sv)`

#### Step 4:
Compare the two directories:
* What tools might exist to compare or find differences?
    * `apropos difference`
    * `apropos compare`
* Let's use `diff`
    * `diff --help`
    * `diff -qr $tb1 $tb2`
    * `diff -qrs $tb1 $tb2 > tbs01-02.diff`
* Use `cat` or `more` to view the file `tbs01-02.diff`
    * **Inline Lab Question #1:**: What's the same? What's different?
    * Peek into some of the files that differ.  
* Find the differences between the single top-level testbench in `$tb1` and the multiple files in `$tb2`:
    * Start from TB1: `cd $tb1` 
    * **Inline Lab Question #2:**: For each of the following - synthesize and/or describe the similarities (if any):
        * `diff --color tinyalu_tb.sv ../03_Interfaces_and_BFMs/top.sv`
        * Repeat this using `tinyalu_tb.sv` for every file listed in `$tb2/tb.f`

#### Step 5:
Run the three part flow on TB1 (`$tb1` if you created the environment variables)

* **Analysis:** 
    1. `vhdlan -kdb -full64 -verbose +vc -cm line+cond+fsm+tgl -f dut.f`
    2. `vlogan -kdb -full64 -verbose +vc -cm line+cond+fsm+tgl -sverilog tinyalu_tb.sv +define+ASSERT_ON+COVER_ON` 
* **Elaborate**
    1. `vcs '-timescale=1ns/1ns' -kdb -full64 -debug_access+all top -lca +vcs+dumpvars  -assert funchier+svaext -cm line+cond+fsm+tgl+assert -sverilog`
* **Simulate**
    1. 10 Iterations: `./simv -cm line+assert+cond+fsm+tgl +NUM+10`
    2. Type `q` and press `enter` to quit
    3. Generate a default report `urg -dir simv.vdb -format both` 
    4. View text files in resulting directory (usually `urgReport`) 
    5. View Dashboard from resulting directory (usually `urgReport`)
        * `cd urgReport`
        * find out what tools exist on this server that could render html? 
            * `apropos website`
            * `apropos web`
            * `apropos html`
        * Use a tool listed above to view the dashboard (at least one name should stand out)    
            *  `<??toolname??> dashboard.html`
            * **Inline Lab Question #3:**: What tool did you end up using?
            * **Inline Lab Question #4:**: Take a screen shot of several of the pages (more than one, less than 5).
        * After exploring quit the tool and return to shell
    6. Enter the Simulator `./simv -R -gui`
        * Open up the `coverage` tool. (Menu Bar > `Tools` > `Coverage`)
        * Load the DB (From Coverage Tool Menu: `File` > `Add/Open Database`):
            * Set `Design Hierarchy/Testbench Location` and/or `Test Directories` to `/home/net/<nid>/courses/eel4932fa23/project1/02_Conventional_Testbench/simv.vdb`
            * Make sure `Test Filter` is set to `*` (all) and that `simv/test` is checked
            * Select `Ok`
        * Explore the interface 
            * You'll likely have one error (relating to FSM coverage)
            * Starting in the Summary Window: 
                * Can you control the columns you see? 
                * For now Drop "FSM" and ADD all the Cover Items (Total, Uncovered, Covered, Excluded)
                * Going back to the Synopsys Documentation, can you find out how the score is computed?
            * Go to the Groups Tab (in Summary)
                * What  high-level groups exist? 
                * Can you find these groups defined in the testbench file? Where (and how) are they defined?
                * Based on the testbench file, how many tests do you expect to be run (in total?) Does it match what we see here?
            * Double click on the `op_set`  coverage group this should open the CovDetail pane on the far right.
                * **Inline Lab Question #5:**
                * Open the group set up and look at the bin names, sort by Hit Count 
                    * what are the least hit `bins` (bottom 3)?
                    * what is/are the most hit `bins` (top 3)?            
                * **Inline Lab Question #6:**: Write down some of the bin names/abbreviations used.

#### Step 6:
Note some of the questions below are similar (or duplicated) of those in the previous section - this is intentional.

Run the three part flow on TB2 (`cd $tb2` if you created the environment variables)

First analyze the directory structure: use `ls` or `tree.`
Recall our work in step 3:
**Inline Lab Question #7:**
1. How does this testbench differ from the last one we saw?
2. Consider, how will we have to modify our analysis phase?


* **Analysis:** 
    1. `vhdlan -kdb -full64 -verbose +vc -cm line+cond+fsm+tgl -f dut.f`
    2.  `vlogan -kdb -full64 -verbose +vc -cm line+cond+fsm+tgl -sverilog -<which flag> <what value> +define+ASSERT_ON+COVER_ON` 
        * Hint #1: Rather than a single testbench file, we have a set of files. 
        * Hint #2: Look at the structure for the `vhdlan` tool which operates on a *.f file
        * Hint #3: Does `vlogan -help` provide insight on using a file to read from instead of a toplevel design element?
* **Elaborate**
    1. `vcs '-timescale=1ns/1ns' -kdb -full64 -debug_access+all top -lca +vcs+dumpvars  -assert funchier+svaext -cm line+cond+fsm+tgl+assert -sverilog`
* **Simulate**
    1. 10 Iterations `./simv -cm line+assert+cond+fsm+tgl +NUM+10`
    2. Type `q` and press `enter` to quit
    3. Generate a default report `urg -dir simv.vdb -format both` 
    4. View text files in resulting directory (usually `urgReport`) 
    5. View Dashboard from resulting directory (usually `urgReport`)
        * `cd urgReport`
        * Use the tool you found in the previous step (Step 5) to view the html dashboard 
            * `<toolname> dashboard.html`
            * Take a screen shot of several of the pages
        * After exploring quit the tool and return to shell
    6. Enter the Simulator `./simv -R -gui`
        * Open up the `coverage` tool. (Menu Bar > `Tools` > `Coverage`)
        * Load the DB (From Coverage Tool Menu: `File` > `Add/Open Database`):
            * Set `Design Hierarchy/Testbench Location` and/or `Test Directories` to `/home/net/<nid>/courses/eel4932fa23/project1/03_Interfaces_and_BFMs/simv.vdb`
            * Make sure `Test Filter` is set to `*` (all) and that `simv/test` is checked
            * Select `Ok`
        * Explore the interface 
            * You'll likely have one error (relating to FSM coverage)
            * Starting in the Summary Window: 
                * For now Drop "FSM" and ADD all the Cover Items (Total, Uncovered, Covered, Excluded)
            * Go to the Groups Tab (in Summary)
                * What  high-level groups exist? 
                * Can you find these groups defined in the testbench file? Where (and how) are they defined?
                * Based on the testbench file, how many tests do you expect to be run (in total?) Does it match what we see here?
            * Double click on the `op_set`  coverage group this should open the CovDetail pane on the far right.                   
                * **Inline Lab Question #8:**
                * Open the group set up and look at the bin names, sort by Hit Count
                    * what are the least hit `bins` (bottom 3)?
                    * what is/are the most hit `bins` (top 3)?     



### Questions:

Create a project report section for Part 2 - include details / questions / responses from completing the procedure above. At a minimum answer the **"Inline Lab Questions X {1-8}"**

After going through the procedure, also answer the following questions - you might need to spend more time with the design and tools!

Based on your exploration above, searching/research online and in the manual

1. Define and then explain the difference between:
    * `functional coverage` (the covers you explored that are written in testbench directly) and
    * `code coverage` (metrics gathered by the simulator e.g., line, toggle, ...)
2. If you rerun your simulation with different values +NUM+XX (XX!=10), do you see changes in:
    * the Functional Coverage (number of tests run?) 
    * the Code Coverage (score change? Toggle/Line change?)
3. If you modify the testbench code in the either directory by removing the `$stop` command do your answers change for:
    * the Functional Coverage (number of tests run?) 
    * the Code Coverage (score change? Toggle/Line change?)

# Fin
