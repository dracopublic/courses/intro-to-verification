# Project 1 

## Part 01: Decoding and Understanding a new design

Featuring VCS & Tiny ALU

### Context & Goal
You've been handed a new design that comes with its own testbench. 
Learn all that you can about the design and the existing testbench.

### Preliminaries

1. Download the following material(s):
    1. TinyALU Code Base (from the UVM Primer Book Website-->GitHub):
        * [The UVM Primer](https://sites.google.com/view/uvmprimer-com/home)
        * [UVM Primer on GitHub](https://github.com/raysalemi/uvmprimer) 
        * Click on "Code" -> Download Zip -- Or directly [here](https://github.com/raysalemi/uvmprimer/archive/refs/heads/master.zip)
        * Rename the file `tinyalu.zip`
2. Copy/Move the zip file to `vlsi.eecs.ucf.edu`
    * Option 1: From a local terminal `scp tinyalu.zip <nid>@vlsi.eecs.ucf.edu:/home/net/<nid>/tinyalu.zip`
    * Option 2: Alternatively use a secure file transfer interface (using vlis.eecs.ucf.edu and port 22) to move the file over
    * Option 3: Copy it from Dr. Mike: `cp /home/net/mi029136/public/tinyalu.zip ~/tinyalu.zip` **Warning this won't always be available**
3. Create course/project directory in your home folder:
    1. `cd ~`
    2. `mkdir -p courses/eel4932fa23/project1`
4. Copy zip file, extract, and rename the folder (if needed):
    1. `cd courses/eel4932fa23/project1`
    2. `cp ~/tinyalu.zip  .`
    3. `unzip tinyalu.zip`
    4.  **If the unzip expands to folder other than "tinyalu" then: ** 
        * `mv <resulting_foldername> tinyalu`

### Project Report

You will submit one project report per group on Webcourses. 
The report consists of responses/materials from each of the parts listed here.

### Objective - Part 1:
Student/learners will be able to:
1. describe the TinyALU's basic functionality (including input, output, and internal functionality)
2. summarize the three main components within the TinyALU's testbench (tester, scoreboard, functional coverage)
3. describe and run the "three stage" process for VCS

### Project Part 1 

#### Step 1
Make sure you have completed the preliminaries above, and as needed, get ready using the [getting started materials](main/docs/getting-started).

#### Step 2
Change to the following working directory (within the tinyalu folder) `~/courses/eel4932fa23/project1/tinyalu/02_Conventional_Testbench`

1. Make note of the folders and files in this directory, write down your best guess for what each file and folder contains (to "peek" inside a file consider using `cat <filename` or `more <filename>`); 
2. Do NOT edit any of these files yet

#### Step 3

Watch the first two YouTube videos from the TinyALU's creator, The UVM Primer book's author, Ray Salemi.
1. [Chapter 1: Introduction and Device Under Test](https://www.youtube.com/watch?v=eeU2zpgXv1A)
2. [Chapter 2: Conventional Testbench for the TinyALU](https://www.youtube.com/watch?v=iX7-41uG8uE)

#### Step 4

Skim Synopsys Documentation on the "Three Step Flow" @ [Synopsys VCS Documentation](https://spdocs.synopsys.com/dow_retrieve/latest/home_public/vcs.html) --> [VCS Documentation - HTML Help](https://spdocs.synopsys.com/dow_retrieve/latest/vg/VCS/vcs_olh/index.htm) --> [VCS Flow](https://spdocs.synopsys.com/dow_retrieve/latest/vg/VCS/vcs_olh/index.htm#page/VCS_User_Guide%2FVCS_MX_Workflow.02.1.htm%23) --> [Three Step Flow](https://spdocs.synopsys.com/dow_retrieve/latest/vg/VCS/vcs_olh/index.htm#page/VCS_User_Guide%2FVCS_MX_Workflow.02.3.htm%23)

* **Inline Lab Question #1:** Write a single sentence definition for each of the three steps in the flow.

#### Step 5

Run the three primary commands needed to **analyze**, **elaborate**, and **simulate** the tinyalu. 
  
##### Analysis
Analyze using `vhdlan` (for vhdl designs) and `vlogan` (for verilog designs)

1. The `dut.f` file appears to contain reference to the design under test folder (`tinyalu_dut/...`)
2. We'd like to run the Synopsys Analyzer (`vhdlan` or `vlogan`) using the following command \
`vhdlan -kdb -full64 -verbose -f dut.f`\
**but before you run an arbitrary command** - you need to understand what it does. 
3. Run `vhdlan -help`
    *  **Inline Lab Question #2:**: Find the four flags mentioned in the command above (`-kdb`, `-full64`, `-verbose`, `-f <filename>`), and briefly summarize what each one does.
4. Run `vhdlan -kdb -full64 -verbose -f dut.f` 
    * **Inline Lab Question #3:**  What new directories/files have been created? 
    * *Hint* Run `ls -ltar` to view and sort items by date.

At this point, you've analyzed the design under test (all the files listed in `dut.f`) but the testbench also needs to be analyzed.

That's a SystemVerilog file, so you're going to use `vlogan` but first let's get a few things ready:

Because we're using synopsys tools there's an additional feature we can and should enable inside our testbench that's currently missing!
1. Double check that you're still in the correct directory `~/courses/eel4932fa23/project1/tinyalu/02_Conventional_Testbench`; if you're not, `cd` to our working directory
2. Add a statement to the `tinyalu_tb.sv` file
    * Open the testbench for editing: `nano tinyalu_tb.sv`
    * Search for (`ctrl-W`) the term *initial* 
        * Cycle through all of the instances until you find the one related to the definition of the *tester*
    * Add a new line immediately after the `initial begin : tester ` statement
    * Add this new text ` $vcdpluson(0,top);`
    * Your new code should look something like this:
    ```verilog
    initial begin : tester
      $vcdpluson(0,top);
      reset_n = 1'b0
    ```
    * Save your file `ctrl-O` (the letter; not zero 0) answer the prompt (`ret`; aka`return` or `enter`) and exit `ctrl-X`
 
3. Now, use the following command to analyze the system verilog based testbench for errors:\
`vlogan -kdb -full64 -sverilog tinyalu_tb.sv`
 

##### Elaborate

The analysis step created some intermediate files - you'll use those to create (compile) an executable simulation (typically called `simv`).

The general syntax for elaboration is `vcs [elab_options] [libname.]design_unit`

We're going to extend this using some debug flags to provide us more details. 

1. Run the following command: `vcs -kdb -debug_access+all -full64 -cm_tgl mda -lca -cm line+cond+fsm+tgl+path top -l vcs-<nid>.log`
2. List the files/directories in your current directory, is `simv` present? 
3. What happens when you take a peek into `simv`?
    * Run `more simv` do you get a warning? 
    * Force it run `cat simv` what do you see?
4. Take a look at the log file you generated `nano vcs-<nid>.log` using `Ctrl-W` or just manually scrolling find out which flag will soon be deprecated.
5. Exit `nano` and return to your directory (`Ctrl-X`)
6. Remove the simulation `rm -rf simv*` and the log file `rm vcs-*.log`
7. Update the command in line 1 by removing the flag for the component that will soon be deprecated. 
    * **Inline Lab Question #4:** Provide the updated command. 

##### Simulate

Now that you have a compiled `simulator` file you're going to use it to simulate the design using  `./simv ` 

###### Option 1:
Run the following command which will combine the last two steps and provide us a gui environment to explore:\
 `vcs -kdb -debug_access+all -full64 -cm_tgl mda -lca -cm line+cond+fsm+tgl top -l vcs.log -R -gui`

###### Option 2 (better):
1. Run a better parameterized `vcs` compilation that dumps information that we'll need:\
  `vcs '-timescale=1ns/1ns' -kdb -full64 -debug_access+all top -lca +vcs+dumpvars  -assert funchier+svaext -cm line+cond+fsm+tgl -sverilog`
2. Then run the simulator (now and anytime later) with `./simv -gui -R`


###### In the GUI

 1. Take some time to explore the interface. Can you:
    * Simulate the Design?
    * Find schematics of the design? How?
        * Can you push down into the design to trace a signal? How?
        * Can you view code associated with a component or signal? How?
    * Can you add all the signal to a wave? How?
    * * **Inline Lab Question #5:** With a Waveform of a simulation answer the following:
        * Zoom in/ goto to and take a screenshot of:
            * clock cycle 0 - what is the op_set, A, B value for you?
            * clock cycle 2023 what are op_set, A, B value for you?
        * If you're in a team - are these values the same or different? Why?

### Questions:

Create a project report section for Part 1 - include details / questions / responses from completing the procedure above. At a minimum answer the **"Inline Lab Questions X {1-5}"**

After going through the procedure, also answer the following questions - you might need to spend more time with the design and tools!

1. Describe the TinyALU's basic functionality (including input, output, and internal functionality)
   * Note the TinyALU is written in VHDL - you're not expected to know VHDL (as if you were a verification engineering and you were handed a design), you need to glean what you can from the designer, and the tool (VCS), and perhaps peering into the code for context hints

2. Summarize the three main components within the TinyALU's testbench (tester, scoreboard, functional coverage); also, using external sources as needed, describe:
   *  constrained random verification
   *  cover groups 

# Fin